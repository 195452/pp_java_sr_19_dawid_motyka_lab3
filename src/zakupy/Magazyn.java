/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zakupy;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;

/**
 *
 * @author Dawid
 */
public class Magazyn {
    
    NumberFormat fmt = NumberFormat.getCurrencyInstance();
    
    ArrayList<Item> rzeczy = new ArrayList<>();

    public int wczytajZPliku(String nazwaPliku) {
        FileReader plik;
        try {
            plik = new FileReader(nazwaPliku);
            BufferedReader in;
            in = new BufferedReader(plik);
            while (in.ready()) {
                String[] linia =  in.readLine().split("`");
                double cena = Double.parseDouble(linia[1]);
                int ilosc = Integer.parseInt(linia[2]);
                rzeczy.add(new Item(linia[0],cena,ilosc));
            }
        } catch (FileNotFoundException ex) {
            System.out.println(ex.toString());
        } catch (IOException ex) {
            System.out.println(ex.toString());
        }
        return 1;
    }

    public int zapiszDoPliku(String nazwaPliku) {
        return 1;
    }

    
    public final String[] naglowkiTabeli = {"Nazwa","Cena","Ilosc w magazynie"};
    public int kup_rzecz(int id)
    {
        return 0;
    }
    public Object[][] doTabeli()
    {
        Object[][] dane = new Object[rzeczy.size()][3];
        for(int i=0;i<rzeczy.size();i++)
        {
            dane[i][0]=rzeczy.get(i).getNazwa();
            dane[i][1]=fmt.format(rzeczy.get(i).getCena());
            dane[i][2]=rzeczy.get(i).getIlosc();
        }
        return dane;
    }
    void dodajRzecz(String nazwa, double cena)
    {
        Item rzecz = new Item("nazwa", cena, 0);
        rzeczy.add(rzecz);
    }
    
    Item getItem(int id)
    {
        return rzeczy.get(id);
    }
    
    
    
    
}
